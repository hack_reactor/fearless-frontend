import { useEffect, useState } from 'react';


const ConferenceForm = ({ locations, getConferences }) => {
    const [formChange, setFormChange] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    });

    const handleFormChange = (e) => {
        setFormChange({
            ...formChange,
            [e.target.name]: e.target.value
            })
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {}
        data.name = formChange.name
        data.starts = formChange.starts
        data.ends = formChange.ends
        data.description = formChange.description
        data.max_presentations = formChange.max_presentations
        data.max_attendees = formChange.max_attendees
        data.location = formChange.location

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type":"application/json"
            }
        }

        const response = await fetch(conferenceUrl, fetchOptions);
        if (response.ok) {
            const data = await response.json();
            setFormChange({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            })
            getConferences();
        }
    }


    return (
        <>
        <div className="container">
            <div className="row-auto">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <form onSubmit={handleSubmit} id="new-conf-form">
                            <h1>Create a new conference</h1>
                            <div>
                                <label htmlFor="name">Name</label>
                                <input onChange={handleFormChange} value={formChange.name} required id="name" className="form-control mb-3" type="text" name="name"></input>
                            </div>
                            <div>
                                <label htmlFor="start-date">Starts</label>
                                <input onChange={handleFormChange} value={formChange.starts} required id="start-date" className="form-control mb-3" type="date" name="starts"></input>
                            </div>
                            <div>
                                <label htmlFor="end-date">Ends</label>
                                <input onChange={handleFormChange} value={formChange.ends} required id="end-date" className="form-control mb-3" type="date" name="ends"></input>
                            </div>
                            <div>
                                <label htmlFor="description">Description</label>
                                <textarea onChange={handleFormChange} value={formChange.description} required id="description" className="form-control mb-3" rows="3" name="description"></textarea>
                            </div>
                            <div>
                                <label htmlFor="max-presentations">Max presentations</label>
                                <input onChange={handleFormChange} value={formChange.max_presentations} required id="max-presentations" type="number" name="max_presentations" className="form-control mb-3"></input>
                            </div>
                            <div>
                                <label htmlFor="max-attendees">Max attendees</label>
                                <input onChange={handleFormChange} value={formChange.max_attendees} required id="max-attendees" type="number" name="max_attendees" className="form-control mb-3"></input>
                            </div>
                            <div>
                                <label htmlFor="select-location">Location</label>
                                <select onChange={handleFormChange} value={formChange.location} id="select-location" className="form-select mb-3" name="location">
                                    <option value="">Select a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>{location.name}</option>
                                        )
                                    })}
                                </select> 
                            </div>
                            <button className="btn btn-primary" type="submit">Create conference</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default ConferenceForm;