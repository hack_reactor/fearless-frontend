import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import { useState, useEffect } from "react";


function App() {
  const [attendees, setAttendees] = useState([]);
  const [conferences, setConferences] = useState([]);
  const [locations, setLocations] = useState([]);
  const [states, setStates] = useState([]);

  const getAttendees = async () => {
    const response = await fetch("http://localhost:8001/api/attendees/");
    if (response.ok) {
      const data = await response.json();
      setAttendees(data.attendees);
    }
  }

  const getConferences = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  const getLocations = async () => {
    const response = await fetch("http://localhost:8000/api/locations/");
    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        
    }
  }

  const getStates = async () => {
    const statesUrl = "http://localhost:8000/api/states";
    const response = await fetch(statesUrl);
    if (response.ok) {
        const data = await response.json();
        setStates(data.states);
        }
    }
  
  useEffect(() => {
    getAttendees();
    getConferences();
    getLocations();
    getStates();
  }, [])
  
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
          <div className="container">
            <Routes>
              <Route index element={<MainPage conferences={conferences}/>} />
              <Route path="locations">
                <Route path="new" element={<LocationForm states={states} getLocations={getLocations}/>} />
              </Route>
              <Route path="conferences">
                <Route path="new" element={<ConferenceForm locations={locations} getConferences={getConferences}/>} />
              </Route>
              <Route path="attendees">
                  <Route index element={<AttendeesList attendees={attendees}/>} />
                  <Route path="new" element={<AttendeeForm conferences={conferences} attendees={getAttendees}/>} />
              </Route>
              <Route path="presentations">
                <Route path="new" element={<PresentationForm conferences={conferences}/>} />
              </Route>
              <Route path="*" element={
                <div>
                  <p>Sorry, you must have found a broken link.</p>
                </div>
              } />
            </Routes>
            {/* <Outlet /> */}
          </div>
      </div>
    </BrowserRouter> 
    </>
  );
}


export default App;
